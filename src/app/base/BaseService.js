/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: BaseService.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/base/BaseService.js
 */

export default class BaseService {
    constructor(props) {
        this.selectCustomer = (props && props.customer) ? props.customer : process.env.VUE_APP_CUSTOMER_ID
        this.appCustomer = process.env.VUE_APP_CUSTOMER_ID
    }

    setCustomer(customer) {
        this.selectCustomer = customer
    }

    makeUrl(url, params) {
        if (params) {
            Object.keys(params).map(param => {
                url = url.replace(':' + param, params[param])
            })
        }

        return url
    }
}
