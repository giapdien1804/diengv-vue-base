/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: Validate.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/base/validate/Validate.js
 */

export class Validate {
    required(val) {
        return (val === '' || val === null)
    }

    isNull(val) {
        if (val === null) return true

        if (Array.isArray(val))
            return val.length > 0

        return !String(val).length > 0
    }

    isMax(val, ...args) {
        if (typeof val === 'number')
            return args[1] ? val >= args[0] : val > args[0]
        else
            return args[1] ? String(val).length >= args[0] : String(val).length > args[0]

        return false
    }

    isMin(val, ...args) {
        if (typeof val === 'number')
            return args[1] ? val <= args[0] : val < args[0]
        else
            return args[1] ? String(val).length <= args[0] : String(val).length < args[0]

        return false
    }

    isNumber(val) {
        return String(val).match(/[0-9]+/)
    }

    isEq(val1, val2) {
        return String(val1) === String(val2)
    }

    isPhone(val, rex) {
        let re = /\d/;
        if (rex)
            re = rex
        return re.test(val);
    }

    isEmail(val) {
        const re = /\S+@\S+\.\S+/;
        return re.test(val);
    }

}

const validate = new Validate()

export default validate
