/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: const.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/const.js
 */

export const DATA_PAGINATE = [30, 50, 80, 100, 150, 200, 250, 400, 500, 1000]
