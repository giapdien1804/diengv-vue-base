/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: auth.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/middleware/register/auth.js
 */
import store from '../../../stores'

export default (to, from, next) => {
    store.dispatch('userSession/checkLogin').then(() => {
        let isLogin = store.getters['userSession/isLogin']

        if (isLogin)
            next()
        else
            next({path: '/auth/login'})
    }).catch(() => {
        next({path: '/auth/login'})
    })
}
