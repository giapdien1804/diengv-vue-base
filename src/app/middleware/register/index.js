import web from './web-guard'
import auth from './auth-guard'

export {web, auth}
