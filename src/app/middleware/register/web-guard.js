/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: web.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/middleware/register/web.js
 */

import store from '../../../stores'

export default (to, from, next) => {
    if (to.path && to.path === '/auth/login') {
        if (store.getters['userSession/isLogin'])
            next(from)
        else
            next()
    } else {
        next()
    }
}
