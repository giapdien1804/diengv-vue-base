/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: errors.mixin.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/mixin/errors.mixin.js
 */

export default {
  data: () => ({
    errors: {}
  }),
  methods: {
    getError(name, group = null) {
      if (this.hasError(name, group))
        if (group)
          return this.errors[group][name]
        else
          return this.errors[name]

      return ''
    },
    hasError(name, group = null) {
      if (group)
        return (group in this.errors) && (name in this.errors[group])
      else
        return name in this.errors
    },
    setError(name, msg, group = null) {
      if (group) {
        if (!(group in this.errors))
          this.$set(this.errors, group, {})
        this.$set(this.errors[group], name, msg)
      } else
        this.$set(this.errors, name, msg)
    },
    removeError(name, group = null) {
      if (group && (group in this.errors) && (name in this.errors))
        delete this.errors[group][name]
      else if (name in this.errors)
        delete this.errors[name]
    },
    resetError(group = null) {
      if (group && (group in this.errors))
        this.errors[group] = {}
      else
        this.errors = {}
    }
  }
}
