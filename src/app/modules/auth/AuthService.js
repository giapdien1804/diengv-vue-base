/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: AuthService.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/modules/auth/AuthService.js
 */

import BaseService from "../../base/BaseService"
import webApi from "../../base/WebApiService"
import {AUTH_URL} from "./enum"
import {i18n} from "../../../config/i18n-setup"

export class AuthService extends BaseService {
    constructor(props) {
        super(props)
    }

    register(user_data) {
        if (!user_data || !user_data.email || !user_data.password)
            return Promise.reject({message: i18n.t('auth.register_error')})

        return webApi.post(AUTH_URL.register)
    }

    login(cer) {
        if (!cer || !cer.email || !cer.password) return Promise.reject('no user data')

        cer.customerId = this.appCustomer

        return webApi.post(AUTH_URL.login, cer)
    }

    logout(user_id) {
        if (!user_id) return Promise.reject('Error...')

        return webApi.post(AUTH_URL.logout, {
            userId: user_id,
            customerId: this.appCustomer
        })
    }
}

const authService = new AuthService()

export default authService
