/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: module.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/modules/auth/module.js
 */

import AppModule from '../module'
import UserSession from './store/user-session'
import Guards from '../../middleware/guards'
import {web} from '../../middleware/register'

export default new AppModule('auth')
    .store([UserSession])
    .routers([
        {
            path: '/auth',
            component: AppModule.layout.DefaultLayout,
            beforeEnter: Guards([web]),
            children: [
                {
                    path: 'login',
                    name: 'auth.login',
                    component: () => import('./views/login'),
                    meta: {bodyClass: 'body-login'}
                },
                {
                    path: 'register',
                    name: 'auth.register',
                    component: () => import('./views/register'),
                    meta: {bodyClass: 'body-register'}
                }
            ]
        }
    ])
    .register()
