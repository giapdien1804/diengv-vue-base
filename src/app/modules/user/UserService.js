/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: UserService.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/modules/user/UserService.js
 */

import {API_URL} from "./enum"
import BaseService from "../../base/BaseService"
import webApiService from '../../base/WebApiService'

export class UserService extends BaseService {
    constructor(props) {
        super(props)
    }

    userInfo(user_id) {
        if (!user_id) return Promise.reject('No user')

        return webApiService.get(this.makeUrl(API_URL["customer.user.info"], {
            ser_id: user_id
        }))
    }

    /**
     * add new user
     * @param user_data instance UserModel.raw
     * @returns {AxiosPromise<UserModel>|Promise<AxiosResponse<UserModel>>}
     */
    addUser(user_data) {
        if (!this.selectCustomer) return Promise.reject('No customer')

        return webApiService.post(API_URL["user.store"],user_data)
    }

    blockUser(user_id) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.post(this.makeUrl(API_URL["user.block"], {
            user_id: user_id
        }))
    }

    removeUser(user_id) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.delete(this.makeUrl(API_URL["user.delete"], {
            user_id: user_id
        }))
    }

    /**
     * update user data
     * @param user_id UserModel._id
     * @param user_data
     * @returns {AxiosPromise<UserModel>|Promise<AxiosResponse<UserModel>>}
     */
    updateUser(user_id, user_data) {
        if (!user_id || !this.selectCustomer) return Promise.reject('No customer or user')

        return webApiService.put(this.makeUrl(API_URL["customer.user.update"], {
            user_id: user_id
        }), user_data)
    }

    /**
     * get all user by customer
     * @param config <{page: number, limit: number, keyword?: string, status?: string, type?: string}>
     * @returns {AxiosPromise<{UserModel[]>|Promise<AxiosResponse<UserModel[]>>}
     */
    allUser(config) {
        if (!this.selectCustomer) return Promise.reject({message: 'No customer'})

        const defaultConfig = {
            current: 1,
            perPage: 50
        }

        return webApiService.get(API_URL["user.all"], {params: {...defaultConfig, ...config}})
    }
}

const userService = new UserService()

export default userService
