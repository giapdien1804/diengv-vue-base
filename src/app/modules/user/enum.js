export const API_URL = {
    'user.all': '/user/all',
    'user.store': '/user/store',
    'user.update': '/user/:user_id/update',
    'user.info': '/user/:user_id/info',
    'user.delete': '/user/:user_id/delete',
    'user.block': '/user/block/:user_id'
}
