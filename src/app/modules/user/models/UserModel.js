/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:58 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: UserModel.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/modules/user/models/UserModel.js
 */

import BaseModel from "../../../base/BaseModel";
import UserProfileModel from "./UserProfileModel";

export default class UserModel extends BaseModel {
    constructor(props) {
        super(props)
        this._id = null
        this.lastName = ''
        this.firstName = ''
        this.email = ''
        this.password = ''
        this.customer = null
        this.birthday = ''
        this.profile = []
        this.status = UserModel.STATUS.inActive
        this.type = UserModel.TYPE.user

        this.syncProps(props)
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`
    }

    syncProps(props) {
        if (props && _.isObject(props) && ('profile' in props) && props.profile.length) {
            props.profile.map(item => new UserProfileModel(item))
        }

        return super.syncProps(props);
    }

    rawValue() {
        const val = super.rawValue();
        if (val.customer)
            val.customer = val.customer._id

        return val
    }
}

UserModel.STATUS = {
    active: 'ACTIVE',
    inActive: 'INACTIVE',
    block: 'BLOCK'
}

UserModel.TYPE = {
    root: 'ROOT',
    admin: 'ADMIN',
    customer: 'CUSTOMER',
    user: 'USER',
}
