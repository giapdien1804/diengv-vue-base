/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:58 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: UserProfileModel.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/modules/user/models/UserProfileModel.js
 */

import BaseModel from "../../../base/BaseModel";

export default class UserProfileModel extends BaseModel {
    constructor(props) {
        super(props)
        // this._id = null
        // this.use_id = null
        this.keyName = ''
        this.keyValue = ''
        this.keyType = UserProfileModel.KEY_TYPE.text

        this.syncProps(props)
    }
}

UserProfileModel.KEY_TYPE = {
    text: 'Text',
    textArea: 'Long text',
    html: 'Html',
    image: 'Image'
}
