/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/9/19 10:49 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: module.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/app/modules/user/module.js
 */

import AppModule from "../module";
import Guards from '../../middleware/guards'
import {auth, web} from '../../middleware/register'

export default new AppModule('user')
    .routers([
        {
            path: '/user',
            component: AppModule.layout.MasterLayout,
            beforeEnter: Guards([web, auth]),
            children: [
                {
                    path: 'manager',
                    name: 'user.manager',
                    component: () => import('./views/user-manager'),
                }
            ]
        }
    ])
    .register()
