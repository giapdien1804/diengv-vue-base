/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/config/index.js
 */

import {loadLanguageAsync} from './i18n-setup'

export default {
    i18nAutoLoad(Vue, option) {
        if (option && option.router && 'beforeEach' in option.router)
            option.router.beforeEach((to, from, next) => {
                if (to.params && to.params.lang) {
                    const lang = to.params.lang
                    loadLanguageAsync(lang).then(() => next())
                } else
                    next()
            })
    }
}
