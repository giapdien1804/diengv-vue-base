/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/helpers/index.js
 */

import * as _ from 'lodash'

const requireModule = require.context('.', true, /\.init.js$/)
requireModule.keys().map(fileName => {
    const module = requireModule(fileName).default
    Object.keys(module).map(name => {
        if (_.has(module[name], 'register')) {
            const mixin = module[name].register(_)
            if (_.isObject(mixin))
                _.mixin(mixin, {'chain': true});
            else
                throw new Error(`Helpers[${name}]: 'register' function must return an object`);
        } else {
            throw new Error(`Helpers[${name}]: Function 'register' not found`);
        }

    })
})


export default _
