/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: env.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/helpers/mixins/env.js
 */

export default {
    register(h) {
        const env = (keyName, default_value = null) => {
            const value = process.env['VUE_APP_' + h.toUpper(h.snakeCase(keyName))]
            return value ? value : default_value
        }

        return {env: env}
    }
}
