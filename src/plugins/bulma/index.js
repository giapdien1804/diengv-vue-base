/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/plugins/bulma/index.js
 */

import BulmaNav from './component/Navbar'
import ModalCard from './component/modal-card'
import Slide from './component/slider'
import NumberInput from './component/number-input'
import FieldInput from './component/field-input'

export default {
  install (Vue, options) {
    Vue.component('bulma-nav', BulmaNav)
    Vue.component('bulma-modal-card', ModalCard)
    Vue.component('bulma-slide', Slide)
    Vue.component('bulma-number-input', NumberInput)
    Vue.component('bulma-field-input', FieldInput)
  }
}
