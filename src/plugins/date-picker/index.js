export default {
    install(Vue, options) {
        Vue.component('bs4-date-picker', () => import('./components/bs4-date-picker'))
    }
}
