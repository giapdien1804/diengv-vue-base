/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/plugins/draggable/index.js
 */

import VueDraggable from './vuedraggable'

export default {
  install(Vue, options) {
    Vue.component('draggable', VueDraggable)
  }
}
