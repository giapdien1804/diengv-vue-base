/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/plugins/index.js
 */

// import Notification from './notification'
import Bulma from './bulma'
import Axios from './axios'
import Draggable from './draggable'
import ContentLoading from './mock-content'
import Dialog from './dialog'

export default (Vue) => {
    Vue.use(Bulma)
    // Vue.use(Notification)
    Vue.use(Axios)
    Vue.use(Draggable)
    Vue.use(ContentLoading)
    Vue.use(Dialog)
}


