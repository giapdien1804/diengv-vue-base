/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: components.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/plugins/mock-content/core/components.js
 */

import MockContentLoading from '../components/MockContent.vue';

import MockCode from '../components/presets/Code.vue';
import MockList from '../components/presets/List.vue';
import MockTwitch from '../components/presets/Twitch.vue';
import MockFacebook from '../components/presets/Facebook.vue';
import MockInstagram from '../components/presets/Instagram.vue';
import MockBulletList from '../components/presets/BulletList.vue';
import MockTable from '../components/presets/Table.vue';

export default MockContentLoading;

export {
    MockCode,
    MockList,
    MockTwitch,
    MockFacebook,
    MockInstagram,
    MockBulletList,
    MockTable,
    MockContentLoading,
};
