/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/plugins/mock-content/index.js
 */

import * as MockComponents from './core/components';
import ColorSwitch from './components/ColorSwitch.vue';

export default {
    install(Vue, options) {
        Object.keys(MockComponents).forEach(c => Vue.component(c, MockComponents[c]));

        Vue.component('ColorSwitch', ColorSwitch);
    }
}
