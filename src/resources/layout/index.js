/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:38 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/resources/layout/index.js
 */

// import Vue from 'vue/types'
import DefaultLayout from './default-layout'
import MasterLayout from './master-layout'
import NewsLayout from './news-layout'
import QuizLayout from './quiz-layout'

/*Vue.component('default-layout', DefaultLayout)
Vue.component('master-layout', MasterLayout)
Vue.component('news-layout', NewsLayout)
Vue.component('quiz-layout', QuizLayout)*/


export {DefaultLayout, MasterLayout, NewsLayout, QuizLayout}
