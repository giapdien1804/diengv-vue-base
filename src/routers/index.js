/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/routers/index.js
 */

import Vue from 'vue'
import Router from 'vue-router'

import {LoadView} from './ulti'
import BodyClass from "./body-class"
import store from '../stores'

Vue.use(Router)

const requireModule = require.context('@/app/modules', true, /module.js$/)
const moduleRouter = []
requireModule.keys().map(fileName => {
    if (fileName !== './module.js') {
        const module = {...requireModule(fileName).default}
        if (module.isActive) {
            if (Array.isArray(module._routers))
                moduleRouter.push(...module._routers)
            else
                moduleRouter.push(module._routers)
        }
    }
})
// console.log(moduleRouter)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'is-active',
    // exact: false,
    routes: [
        {
            path: "*",
            component: LoadView('errors/page-not-found')
        },
        ...moduleRouter
    ]
})

router.beforeEach((to, from, next) => {
    store.dispatch('updatePrevRouter', from)

    next()
})

new BodyClass(router)

export default router
