/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: ulti.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/routers/ulti.js
 */

export const LoadComponent = (path) => () => import(`@/${path}.vue`)

export const LoadView = (name, index = false) =>
    LoadComponent(`resources/views/${name}${index ? '/index' : ''}`)

