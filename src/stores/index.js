/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/stores/index.js
 */

import Vue from 'vue'
import Vuex from 'vuex'
import Modules from './modules'
import MutationType from './mutations-type'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {...Modules},
    strict: process.env.NODE_ENV !== 'production',
    state: {
        isOnline: true,
        prevRoute: '/'
    },
    mutations: {
        [MutationType.updateOnlineFlag](state, flag) {
            state.isOnline = flag
        },
        [MutationType.updatePrevRouter](state, router) {
            state.prevRoute = router
        }
    },
    actions: {
        updateOnlineFlag({commit}, flag) {
            commit(MutationType.updateOnlineFlag, flag)
        },
        updatePrevRouter({commit}, router){
            commit(MutationType.updatePrevRouter, router)
        }
    }
})
