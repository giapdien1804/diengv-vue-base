/*
 * Copyright (c) 2019.
 * The project framework is designed by DIENGV. This is an open project, with no commercial properties.
 *  LastModified: 6/5/19 9:31 PM
 *  Author: diengv < Giáp Văn Điện >
 *  Email: diengv.dev@gmail.com
 *  File name: index.js
 *  File path: D:/Projects/diengiap/express-gds-client/src/stores/modules/index.js
 */

const requireModule = require.context('.', true, /\.js$/)
const modules = {}
const accessFile = ['actions.js', 'getters.js', 'mutations.js', 'state.js']

requireModule.keys().map(fileName => {
    if (fileName === 'module.js.js') return

    const moduleDefault = {
        namespaced: true
    }
    // get all module
    const namePath = fileName.split('/')
    const folderName = _.nth(namePath, 1)

    if (accessFile.indexOf(_.last(namePath)) === -1) return

    if (folderName) {
        let moduleName = null
        if (!_.endsWith(folderName, '.js')) {
            moduleName = _.camelCase(
                folderName.replace(/(\.\/|\.js)/g, '')
            )

            if (!(moduleName in modules))
                modules[moduleName] = moduleDefault
        }

        if (moduleName in modules) {
            const storeObjName = _.last(namePath).replace(/(\.\/|\.js)/g, '')
            const obj = {}
            obj[storeObjName] = {...requireModule(fileName).default}
            _.assignIn(modules[moduleName], obj)
        }
    }
})

export default modules
